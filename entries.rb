require 'csv'
require 'open-uri'

begin
  
  games = CSV.parse(open("http://beta.nopaystation.com/tsv/PSV_GAMES.tsv"), { :col_sep => "\t", :headers => true, encoding: "ISO8859-1"})
  demos = CSV.parse(open("http://beta.nopaystation.com/tsv/PSV_DEMOS.tsv"), { :col_sep => "\t", :headers => true, encoding: "ISO8859-1"})


result = ""
Dir["PCS*/*.ppk"].each do |game|
	game_title_id = game.split("/")[0]
  begin
    game_name = games.find {|i| i["Title ID"] == game.split("/")[0] }["Name"]
  rescue
    game_name = demos.find {|i| i["Title ID"] == game.split("/")[0] }["Name"]
  end
	result += "#{game}=#{game_name}\n"
end

File.write("entries.txt", result)
rescue
end
